<?php

require "model/dao/teachrDao.php";
require "utils/jsonFormatter.php";

/* Main controller */

// Project globals
$ROOT_PATH = dirname(dirname(__FILE__));
$PROJECT_PATH = "Teachr/teachr";

// Start session
session_start();

// Display views
function displayHome() {
    global $ROOT_PATH;

    // Display home.php
    require("$ROOT_PATH/view/home.php");
}

function displayFaculty() {
    global $ROOT_PATH;

    // Get all faculties
    $target = "faculty";
    $data_json = getJSONFaculty(teachrDao::getFaculties());
    $json = "{\"path\": [\"faculty\", \"domain\", \"course\"], \"data\": " . $data_json . "}";
    $data = json_decode($data_json);

    // Display tree.php
    require("$ROOT_PATH/view/tree.php");
}

function displayDomain() {
    global $ROOT_PATH;

    // Get all domains
    $target = "domain";
    $data_json = getJSONDomain(teachrDao::getDomains());
    $json = "{\"path\": [\"domain\", \"course\"], \"data\": " . $data_json . "}";
    $data = json_decode($data_json);

    // Display tree.php
    require("$ROOT_PATH/view/tree.php");
}

function displayProfessor() {
    global $ROOT_PATH;

    // Get all domains
    $target = "professor";
    $data_json = getJSONProfessor(teachrDao::getProfessors());
    $json = "{\"path\": [\"professor\", \"course\"], \"data\": " . $data_json . "}";
    $data = json_decode($data_json, true);

    // Display tree.php
    require("$ROOT_PATH/view/tree.php");
}

function displayCycle() {
    global $ROOT_PATH;

    // Get all domains
    $target = "cycle";
    $data_json = "{\"1\": \"Cycle 1 - Bachelier\", \"2\": \"Cycle 2 - Master\"}";
    $json = "{\"path\": [\"cycle\", \"course\"], \"data\": " . $data_json . "}";
    $data = json_decode($data_json);

    // Display tree.php
    require("$ROOT_PATH/view/tree.php");
}

// Get JSON responses
function getDomainsByFaculty($faculty_id) {
    $data = teachrDao::getDomainsByFaculty($faculty_id);
    $json = getJSONDomain($data);
    print $json;
}

function getCoursesByDomain($domain_id) {
    $data = teachrDao::getCoursesByDomain($domain_id);
    $json = getJSONCourse($data);
    print $json;
}

function getCoursesByProfessor($professor_id) {
    $data = teachrDao::getCoursesByProfessor($professor_id);
    $json = getJSONCourse($data);
    print $json;
}

function getCoursesByDomainFromFaculty($faculty_id, $domain_id) {
    $data = teachrDao::getCoursesByDomainFromFaculty($faculty_id, $domain_id);
    $json = getJSONCourse($data);
    print $json;
}

function getCoursesByCycle($cycle_id) {
    $cycle = ($cycle_id == "1") ? "Cycle 1 - Bachelier" : "Cycle 2 - Master";
    $data = teachrDao::getCoursesByCycle($cycle);
    $json = getJSONCourse($data);
    print $json;
}

// Routes
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['target'])) {
        switch ($_GET['target']) {
            /* faculty */
            case 'faculty':
                if (isset($_GET['node']) && isset($_GET['node_id'])) {
                    switch ($_GET['node']) {
                        case 'domain':
                            getDomainsByFaculty($_GET['node_id']);
                            break;
                        case 'course':
                            if (isset($_GET['target_id'])) {
                                getCoursesByDomainFromFaculty($_GET['target_id'], $_GET['node_id']);
                            }
                            break;
                    }
                } else {
                    displayFaculty();
                }
                break;

            /* domain */
            case 'domain':
                if (isset($_GET['node']) && isset($_GET['node_id'])) {
                    switch ($_GET['node']) {
                        case 'course':
                            getCoursesByDomain($_GET['node_id']);
                            break;
                    }
                } else {
                    displayDomain();
                }
                break;

            /* professor */
            case 'professor':
                if (isset($_GET['node']) && isset($_GET['node_id'])) {
                    switch ($_GET['node']) {
                        case 'course':
                            getCoursesByProfessor($_GET['node_id']);
                            break;
                    }
                } else {
                    displayProfessor();
                }
                break;

            /* cycle */
            case 'cycle':
                if (isset($_GET['node']) && isset($_GET['node_id'])) {
                    switch ($_GET['node']) {
                        case 'course':
                            getCoursesByCycle($_GET['node_id']);
                            break;
                    }
                } else {
                    displayCycle();
                }
                break;
        }
    } else {
        displayHome();
    }
}

?>