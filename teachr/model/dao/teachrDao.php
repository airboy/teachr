<?php

class TeachrDao {

    private static $conn;
    private static $username = 'dynabrain_teachr';
    private static $password = 'webbike==25';


    private static function getConnection() {
        try {
            self::$conn = new PDO('mysql:host=mysql2.alwaysdata.com;dbname=dynabrain_db', self::$username, self::$password);
            self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    /*** FACULTY ***/

    // Return all faculties.
    public static function getFaculties() {
        self::getConnection();
        try {
            $data = self::$conn->query('SELECT * FROM faculty');
            return $data->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return a specific faculty.
    public static function getFaculty($id) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM faculty WHERE faculty_id=:id');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll()[0];
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    /*** DOMAIN ***/

    // Return all domains.
    public static function getDomains() {
        self::getConnection();
        try {
            $data = self::$conn->query('SELECT * FROM domain');
            return $data->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return a specific domain.
    public static function getDomain($id) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM domain WHERE domain_id=:id');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll()[0];
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return all domains for a specific faculty.
    public static function getDomainsByFaculty($faculty_id) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT DISTINCT(d.domain_id), d.title  FROM domain AS d INNER JOIN course AS c ON d.domain_id=c.target_domain WHERE c.course_id IN (SELECT target_course FROM course_faculty WHERE target_faculty=:faculty_id)');
            $stmt->bindParam(':faculty_id', $faculty_id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    /*** COURSE ***/

    // Return all courses.
    public static function getCourses() {
        self::getConnection();
        try {
            $data = self::$conn->query('SELECT * FROM course');
            return $data->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return a specific course.
    public static function getCourse($id) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM course WHERE course_id=:id');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll()[0];
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return all courses for a specific domain.
    public static function getCoursesByDomain($domain_id) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM course WHERE target_domain=:domain_id');
            $stmt->bindParam(':domain_id', $domain_id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return all courses for a specific domain within a specific faculty.
    public static function getCoursesByDomainFromFaculty($faculty_id, $domain_id) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM course WHERE target_domain=:domain_id AND course_id IN (SELECT target_course FROM course_faculty  WHERE target_faculty=:faculty_id)');
            $stmt->bindParam(':domain_id', $domain_id, PDO::PARAM_INT);
            $stmt->bindParam(':faculty_id', $faculty_id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return all courses for a specific professor.
    public static function getCoursesByProfessor($professor_id) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM course WHERE course_id IN (SELECT target_course FROM course_professor WHERE target_professor=:professor_id)');
            $stmt->bindParam(':professor_id', $professor_id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return all courses for a specific cycle.
    public static function getCoursesByCycle($cycle) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM course WHERE cycle=:cycle');
            $stmt->bindParam(':cycle', $cycle, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    /*** PROFESSOR ***/

    // Return all professors.
    public static function getProfessors() {
        self::getConnection();
        try {
            $data = self::$conn->query('SELECT * FROM professor');
            return $data->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return a specific professor.
    public static function getProfessor($id) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM professor WHERE professor_id=:id');
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll()[0];
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return all professors for a specific domain.
    public static function getProfessorsByDomain($domain_id) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM professor WHERE professor_id IN (SELECT cp.target_professor FROM course AS c INNER JOIN course_professor AS cp ON c.course_id=cp.target_course WHERE c.target_domain=:domain_id)');
            $stmt->bindParam(':domain_id', $domain_id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return all professors for a specific faculty.
    public static function getProfessorsByFaculty($faculty_id) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM professor WHERE professor_id IN (SELECT target_professor from professor_faculty WHERE target_faculty=:faculty_id)');
            $stmt->bindParam(':faculty_id', $faculty_id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return all professors for a specific faculty.
    public static function getProfessorsByCourse($course_id) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM professor WHERE professor_id IN (SELECT target_professor FROM course_professor WHERE target_course=:course_id)');
            $stmt->bindParam(':course_id', $course_id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

    // Return all courses for a specific cycle.
    public static function getProfessorsByCycle($cycle) {
        self::getConnection();
        try {
            $stmt = self::$conn->prepare('SELECT * FROM professor WHERE professor_id IN (SELECT cp.target_professor FROM course_professor AS cp INNER JOIN course AS c ON cp.target_course=c.course_id WHERE c.cycle=:cycle)');
            $stmt->bindParam(':cycle', $cycle, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetchAll();
>>>>>>> 17bdf8e9e33cc48f2d7a30d971cb128b33e00ef7
        } catch(PDOException $e) {
            // Print PDOException message
            echo $e->getMessage();
        }
    }

}

?>