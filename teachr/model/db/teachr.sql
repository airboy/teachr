/* TEACHR SQL SCRIPT */

USE dynabrain_db;

CREATE TABLE faculty (
    faculty_id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(45) NOT NULL
)TYPE=InnoDB;

CREATE TABLE domain (
    domain_id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(45) NOT NULL
)TYPE=InnoDB;

CREATE TABLE professor (
    professor_id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    fname VARCHAR(30) NOT NULL,
    email VARCHAR(100) NOT NULL
)TYPE=InnoDB;

CREATE TABLE course (
    course_id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(60) NOT NULL,
    langue VARCHAR(30) NOT NULL,
    cycle VARCHAR(30) NOT NULL,
    level INT NOT NULL,
    target_domain INT NOT NULL,
    CONSTRAINT FOREIGN KEY (target_domain) REFERENCES domain (domain_id)
)TYPE=InnoDB;

CREATE TABLE professor_faculty (
    professor_faculty_id INT PRIMARY KEY AUTO_INCREMENT,
    target_professor INT NOT NULL,
    target_faculty INT NOT NULL,
    CONSTRAINT FOREIGN KEY (target_professor) REFERENCES professor (professor_id),
    CONSTRAINT FOREIGN KEY (target_faculty) REFERENCES faculty (faculty_id)
)TYPE=InnoDB;

CREATE TABLE course_professor (
    course_professor_id INT PRIMARY KEY AUTO_INCREMENT,
    target_course INT NOT NULL,
    target_professor INT NOT NULL,
    CONSTRAINT FOREIGN KEY (target_course) REFERENCES course (course_id),
    CONSTRAINT FOREIGN KEY (target_professor) REFERENCES professor (professor_id)
)TYPE=InnoDB;

CREATE TABLE course_faculty (
    course_faculty_id INT PRIMARY KEY AUTO_INCREMENT,
    target_course INT NOT NULL,
    target_faculty INT NOT NULL,
    CONSTRAINT FOREIGN KEY (target_course) REFERENCES course (course_id),
    CONSTRAINT FOREIGN KEY (target_faculty) REFERENCES faculty (faculty_id)
)TYPE=InnoDB;

/* INSERTS */
/* faculty */
INSERT INTO faculty VALUES ('', 'Droit et criminologie');
INSERT INTO faculty VALUES ('', 'Ecole polytechnique de Bruxelles');
INSERT INTO faculty VALUES ('', 'Médecine');
INSERT INTO faculty VALUES ('', 'Sciences');

/* domain */
INSERT INTO domain VALUES ('', 'Droit');
INSERT INTO domain VALUES ('', 'Criminologie');
INSERT INTO domain VALUES ('', 'Sciences médicales');
INSERT INTO domain VALUES ('', 'Chimie');
INSERT INTO domain VALUES ('', 'Informatique');
INSERT INTO domain VALUES ('', 'Mathématiques');
INSERT INTO domain VALUES ('', 'Physique');

/* professor */
INSERT INTO professor VALUES ('', 'Massager', 'Nathalie', 'nmassager@ulb.ac.be');
INSERT INTO professor VALUES ('', 'Defraene', 'Dominique', 'ddefraene@ulb.ac.be');
INSERT INTO professor VALUES ('', 'Waelbroeck', 'Magali', 'mwaelbroeck@ulb.ac.be');
INSERT INTO professor VALUES ('', 'Brion', 'Jean-Pierre', 'jp.brion@ulb.ac.be');
INSERT INTO professor VALUES ('', 'Noel', 'Jean-Christophe', 'jc.noel@ulb.ac.be');
INSERT INTO professor VALUES ('', 'Massart', 'Thierry', 'tmassart@ulb.ac.be');
INSERT INTO professor VALUES ('', 'Sparenberg', 'Jean-Marc', 'jc.sparenberg@ulb.ac.be');
INSERT INTO professor VALUES ('', 'De Smet', 'Yves', 'yves.desmet@ulb.ac.be');
INSERT INTO professor VALUES ('', 'Fortz', 'Bernard', 'bfortz@ulb.ac.be');
INSERT INTO professor VALUES ('', 'Fiorini', 'Samuel', 'sfiorini@ulb.ac.be');
INSERT INTO professor VALUES ('', 'Langerman', 'Stefan', 'slangerman@ulb.ac.be');

/* professor_faculty */
INSERT INTO professor_faculty VALUES ('', 1, 1);
INSERT INTO professor_faculty VALUES ('', 2, 1);
INSERT INTO professor_faculty VALUES ('', 3, 3);
INSERT INTO professor_faculty VALUES ('', 4, 3);
INSERT INTO professor_faculty VALUES ('', 5, 3);
INSERT INTO professor_faculty VALUES ('', 6, 2);
INSERT INTO professor_faculty VALUES ('', 6, 4);
INSERT INTO professor_faculty VALUES ('', 7, 4);
INSERT INTO professor_faculty VALUES ('', 8, 2);
INSERT INTO professor_faculty VALUES ('', 9, 4);
INSERT INTO professor_faculty VALUES ('', 10, 4);
INSERT INTO professor_faculty VALUES ('', 11, 4);

/* course */
INSERT INTO course VALUES ('', 'Introduction au droit civil', 'Francais', 'Cycle 1 - Bachelier', 1, 1);
INSERT INTO course VALUES ('', 'Méthodologie de la criminologie', 'Francais', 'Cycle 2 - Master', 2, 2);
INSERT INTO course VALUES ('', 'Questions approfondies de criminologie et de victimologie', 'Francais', 'Cycle 2 - Master', 2, 2);
INSERT INTO course VALUES ('', 'Biochimie générale', 'Francais', 'Cycle 1 - Bachelier', 1, 3);
INSERT INTO course VALUES ('', 'Histologie générale', 'Francais', 'Cycle 1 - Bachelier', 2, 3);
INSERT INTO course VALUES ('', 'Anatomie pathologique générale', 'Francais', 'Cycle 1 - Bachelier', 3, 3);
INSERT INTO course VALUES ('', 'Informatique', 'Francais', 'Cycle 1 - Bachelier', 1, 5);
INSERT INTO course VALUES ('', 'Physique quantique et statistique', 'Francais', 'Cycle 1 - Bachelier', 2, 7);
INSERT INTO course VALUES ('', 'Algorithmique et recherche opérationnelle', 'Francais', 'Cycle 1 - Bachelier', 3, 5);
INSERT INTO course VALUES ('', 'Programmation 1', 'Francais', 'Cycle 1 - Bachelier', 1, 5);
INSERT INTO course VALUES ('', 'Algorithmique 2', 'Francais', 'Cycle 1 - Bachelier', 2, 5);
INSERT INTO course VALUES ('', 'Mathématiques discrètes', 'Francais', 'Cycle 1 - Bachelier', 3, 6);
INSERT INTO course VALUES ('', 'Introduction to language theory and compilation', 'English', 'Cycle 2 - Master', 1, 5);
INSERT INTO course VALUES ('', 'Computational geometry', 'English', 'Cycle 2 - Master', 2, 5);

/* course_professor */
INSERT INTO course_professor VALUES ('', 1, 1);
INSERT INTO course_professor VALUES ('', 2, 2);
INSERT INTO course_professor VALUES ('', 3, 2);
INSERT INTO course_professor VALUES ('', 4, 3);
INSERT INTO course_professor VALUES ('', 5, 4);
INSERT INTO course_professor VALUES ('', 6, 5);
INSERT INTO course_professor VALUES ('', 7, 6);
INSERT INTO course_professor VALUES ('', 8, 7);
INSERT INTO course_professor VALUES ('', 9, 8);
INSERT INTO course_professor VALUES ('', 9, 9);
INSERT INTO course_professor VALUES ('', 10, 6);
INSERT INTO course_professor VALUES ('', 11, 9);
INSERT INTO course_professor VALUES ('', 12, 10);
INSERT INTO course_professor VALUES ('', 13, 6);
INSERT INTO course_professor VALUES ('', 14, 11);

/* course_faculty */
INSERT INTO course_faculty VALUES ('', 1, 1);
INSERT INTO course_faculty VALUES ('', 2, 1);
INSERT INTO course_faculty VALUES ('', 3, 1);
INSERT INTO course_faculty VALUES ('', 4, 3);
INSERT INTO course_faculty VALUES ('', 5, 3);
INSERT INTO course_faculty VALUES ('', 6, 3);
INSERT INTO course_faculty VALUES ('', 7, 2);
INSERT INTO course_faculty VALUES ('', 8, 2);
INSERT INTO course_faculty VALUES ('', 9, 2);
INSERT INTO course_faculty VALUES ('', 9, 4);
INSERT INTO course_faculty VALUES ('', 10, 4);
INSERT INTO course_faculty VALUES ('', 11, 4);
INSERT INTO course_faculty VALUES ('', 12, 2);
INSERT INTO course_faculty VALUES ('', 12, 4);
INSERT INTO course_faculty VALUES ('', 13, 2);
INSERT INTO course_faculty VALUES ('', 13, 4);
INSERT INTO course_faculty VALUES ('', 14, 2);
INSERT INTO course_faculty VALUES ('', 14, 4);