<!DOCTYPE html>
<html>
    <head>
        <title>Teach'R | <?php echo ucfirst($target) ?></title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="teachr" />
        <meta name="author" content="airboy" />
        <link href="media/css/base.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container" data-target="faculty">
            <div class="row" id="header">
                <div class="span12">
                    <div id="logo-container">
                        <a href="/Teachr/teachr/">
                            <img src="media/img/logo.png" id="logo" alt="Teach'r logo">
                        </a>
                    </div>
                </div>
            </div>

            <div class="row" id="body-wrapper">
                <div class="span12">
                    <div id="tree" data-tree='<?php echo $json ?>'></div>
                    <div id="data-legend">
                        <p id="data-error"></p>
                        <h3>LEGEND</h3>
                        <ul class="unstyled" id="legend-container">
                            <li class="legend-item" data-node="<?php echo $target ?>">
                                <h4 class="<?php echo $target ?>"><?php echo strtoupper($target) ?></h4>
                                <ul class="unstyled" id="legend-item-container">
                                    <li class="legend-item-title">
                                        <?php
                                        if ($target != 'professor') {
                                            foreach ($data as $key => $value) {
                                        ?>
                                        <span class="legend-title"><strong><span class="<?php echo 'lengend-number ' . $target ?>"><?php echo $key ?></span> : <?php echo $value ?></strong></span>
                                        <?php
                                            }
                                        } else {
                                        ?>
                                        <table class="table table-bordered">
                                            <thead>
                                                <th>ID</th>
                                                <th>NAME</th>
                                                <th>FIRSTNAME</th>
                                                <th>EMAIL</th>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($data as $key => $value) {
                                            ?>
                                                <tr><td class="professor"><?php echo $key ?></td><td><?php echo strtoupper($value['name']) ?></td><td><?php echo $value['fname'] ?></td><td><?php echo $value['email'] ?></td></tr>
                                            <?php
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <?php
                                        }
                                        ?>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- /end #data-legend -->
                    <div id="course-container">
                        <h4 class="course">COURSE</h4>
                        <table id="course-results" class="table table-bordered">
                            <thead>
                                <th>ID</th>
                                <th>TITLE</th>
                                <th>LANGUE</th>
                                <th>CYCLE</th>
                                <th>LEVEL</th>
                                <th>DOMAIN</th>
                                <th>PROFESSOR(S)</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row" id="footer">
                <div class="span12">
                    <span><strong>TEACH'R - <i class="grey">2013</i> - <i class="blue">version : 0.1.0</i></strong></span>
                </div>
            </div>
        </div>

        <!-- Scripts imports -->
        <script type="text/javascript" src="media/js/global/jquery.js"></script>
        <script type="text/javascript" src="media/js/vendor/kinetic.min.js"></script>

        <!-- Custom scripts imports -->
        <script type="text/javascript" src="media/js/tree.js"></script>
        <script type="text/javascript" src="media/js/base.js"></script>

    </body>
</html>