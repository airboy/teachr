<!DOCTYPE html>
<html>
    <head>
        <title>Teach'R | Home</title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta name="keywords" content="teachr" />
        <meta name="author" content="airboy" />
        <link href="media/css/base.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="row" id="header">
                <div class="span12">
                    <div id="logo-container">
                        <a href=<?php echo $PROJECT_PATH ?>>
                            <img src="media/img/logo.png" id="logo" alt="Teach'r logo">
                        </a>
                    </div>
                </div>
            </div>

            <div class="row" id="body-wrapper">
                <div class="span12">
                    <ul id="node-container" class="unstyled">
                        <li class="node">
                            <a href=<?php echo $PROJECT_PATH . "?target=faculty" ?>>
                                <div class="circle">
                                    <div class="letter letter-faculty"></div>
                                </div>
                                <span class="circle-title"><strong>FACULTY</strong></span>
                            </a>
                        </li>
                        <li class="node">
                            <a href=<?php echo $PROJECT_PATH . "?target=domain" ?>>
                                <div class="circle">
                                    <div class="letter letter-domain"></div>
                                </div>
                                <span class="circle-title"><strong>DOMAIN</strong></span>
                            </a>
                        </li>
                        <li class="node">
                            <a href=<?php echo $PROJECT_PATH . "?target=professor" ?>>
                                <div class="circle">
                                    <div class="letter letter-professor"></div>
                                </div>
                                <span class="circle-title"><strong>PROFESSOR</strong></span>
                            </a>
                        </li>
                        <li class="node">
                            <a href=<?php echo $PROJECT_PATH . "?target=cycle" ?>>
                                <div class="circle">
                                    <div class="letter letter-course"></div>
                                </div>
                                <span class="circle-title"><strong>CYCLE</strong></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row" id="footer">
                <div class="span12">
                    <span><strong>TEACH'R - <i class="grey">2013</i> - <i class="blue">version : 0.1.0</i></strong></span>
                </div>
            </div>
        </div>
    </body>
</html>
