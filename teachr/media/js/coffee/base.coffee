### Base coffee to launch teachr tree canvas
@Author : R. FONCIER
@Date : 30/04/2013
###

# Colors
colors =
    faculty: '#15A0EB'
    domain: '#1BE07E'
    course: '#F2D61D'
    cycle: '#E01BB9'
    professor: '#FA8B0C'

# Kinetic setup
data = $('#tree').data('tree')
error = $('#data-legend #data-error')
legend = $('#legend-container')
course = $('#course-container')
tree = new Tree(data, colors, error, legend, course)
tree.constructFirstLevel()