### Tree coffee to manage teachr tree canvas
@Author : R. FONCIER
@Date : 02/05/2013
###

# Tree globals
RADIUS = 30
SPACE = 20
INTERVAL = 0

# Tree is a static class which allows to create tree in Javascript and navigate through it.
class window.Tree

    @data = null
    @colors = null
    @target = null
    @root = null
    @stage = null
    @layer = null
    @error = null
    @legend = null
    @course = null

    # Initialize navigation variables
    @currentNode = 1
    @currentNodeLevel = 0
    @depth = 0

    constructor: (data, colors, error_container, legend_container, course_table) ->
        Tree.stage = new Kinetic.Stage(
            container: 'tree'
            width: 940
            height: 240
        )

        Tree.layer = new Kinetic.Layer()

        Tree.root = Tree.createNode(Tree.stage.getWidth() / 2, (RADIUS + 10), 1, "R")
        Tree.layer.add(Tree.root)
        Tree.stage.add(Tree.layer)

        # Set static variables.
        Tree.data = data
        Tree.colors = colors
        Tree.error = error_container
        Tree.legend = legend_container
        Tree.course = course_table

    @getRoot: () ->
        @root

    @getData: () ->
        @data

    @getTarget: () ->
        @target

    @getColors: () ->
        @colors

    constructFirstLevel: () ->
        Tree.target = Tree.data["path"][Tree.depth]
        Tree.addSubtree(Tree.getRoot(), Tree.data['data'], Tree.colors[Tree.target])

    @add: (node) ->
        # Fetch data to server and get JSON response
        node_attrs = node.getId().split("_")
        node_target = Tree.data["path"][node_attrs[0]]
        node_id = node_attrs[1]

        # URI to request
        path = ""
        if node_target is 'course'
            target_id = node.getParent().getParent().getParent().getId().split("_")[1]
            path = '/Teachr/teachr/?target='+Tree.getTarget()+'&node='+node_target+'&node_id='+node_id+'&target_id='+target_id
        else
            path = '/Teachr/teachr/?target='+Tree.getTarget()+'&node='+node_target+'&node_id='+node_id

        # fetch data
        $.get path,
            (data) ->
                console.log "data fetched : "+data
                data = JSON.parse data

                # Process data
                if node_target is 'course'
                    if data.length > 0
                        Tree.addLastSubtree(node, data, Tree.colors[node_target])
                        Tree.displayCourse(node_target, data)
                    else
                        Tree.displayError("No data found")
                else
                    if Object.keys(data).length > 0
                        Tree.addSubtree(node, data, Tree.colors[node_target])
                        Tree.addLegend(node_target, data)
                    else
                        Tree.displayError("No data found")
                true

    @remove: (node) ->
        # Remove all children from this node.
        if node.getChildren()[0] instanceof Kinetic.Group
            Tree.currentNode = node.getParent().getParent().getParent()
            Tree.stage.setHeight(Tree.stage.getHeight() - 164) if Tree.depth > 1
            Tree.currentNodeLevel--
            Tree.depth--
            node.getChildren()[0].remove()
            Tree.layer.draw()
            Tree.removeLegend(Tree.data["path"][node.getId().split("_")[0]])
            Tree.removeCourse()

    @addSubtree: (currentNode, data, color) ->
        # Set current params
        Tree.currentNode = currentNode
        Tree.currentNodeLevel = Tree.depth
        Tree.depth++
        Tree.stage.setHeight(Tree.stage.getHeight() + 164) if Tree.depth > 1
        interval = 0
        node_space = if Object.keys(data).length < 10 then SPACE else SPACE / 2

        # Define X coordinate to start drawing
        x_start = currentNode.getChildren()[0].getX() - (node_space + (RADIUS + 3)) * (Object.keys(data).length - 1)

        # Create a group for children
        children_group = new Kinetic.Group()

        # Iterate through data and create node + label + branch
        $.each data, (key, value) ->
            # Create group
            branch_group = new Kinetic.Group()

            # Create node
            node_group = Tree.createNode(x_start + interval, (currentNode.getChildren()[0].getY() + 164), key)

            # Bind events on node
            node_group.on 'mouseover', () ->
                document.body.style.cursor = "pointer"
                if this.getChildren()[0] instanceof Kinetic.Circle
                    this.getChildren()[0].setStroke(color)
                    this.getChildren()[0].setStrokeWidth(5)
                    this.getParent().getChildren()[0].setStrokeWidth(5)
                    this.getChildren()[1].setFill(color)
                else
                    this.getChildren()[1].setStroke(color)
                    this.getChildren()[1].setStrokeWidth(5)
                    this.getParent().getChildren()[0].setStrokeWidth(5)
                    this.getChildren()[2].setFill(color)
                Tree.layer.draw()

            node_group.on 'mouseout', () ->
                document.body.style.cursor = "default"
                if this.getChildren()[0] instanceof Kinetic.Circle
                    this.getChildren()[0].setStroke("#999")
                    this.getChildren()[0].setStrokeWidth(3)
                    this.getParent().getChildren()[0].setStrokeWidth(3)
                    this.getChildren()[1].setFill("#222")
                else
                    this.getChildren()[1].setStroke("#999")
                    this.getChildren()[1].setStrokeWidth(3)
                    this.getParent().getChildren()[0].setStrokeWidth(3)
                    this.getChildren()[2].setFill("#222")
                Tree.layer.draw()

            node_group.on 'click', (event) ->
                # Set cancelBubble to true allows to avoid event calls in cascade.
                event.cancelBubble = true

                if this.getId() is Tree.currentNode.getId()
                    Tree.remove(this)
                else
                    tLevel = parseInt(this.getId().split("_")[0])
                    console.log "Levels : " + Tree.currentNodeLevel + " - " + tLevel
                    if tLevel > Tree.currentNodeLevel
                        Tree.add(this)
                    else
                        if tLevel is Tree.currentNodeLevel
                            #console.log "test"
                            Tree.remove(Tree.currentNode)
                            Tree.add(this)
                        else
                            Tree.remove(this)

            # Draw branch
            branch = new Kinetic.Line(
                points: [node_group.getChildren()[0].getX(), node_group.getChildren()[0].getY(), currentNode.getChildren()[0].getX(), currentNode.getChildren()[0].getY()]
                stroke: color
                strokeWidth: 3
                lineCap: 'round'
                lineJoin: 'round'
            )

            branch_group.add(branch)
            branch_group.add(node_group)
            children_group.add(branch_group)

            # Update interval between node
            interval += (node_space*2) + (RADIUS + 3)*2

        # Layer redraw
        currentNode.add(children_group)
        children_group.moveToBottom()
        Tree.layer.draw()

    @addLastSubtree: (currentNode, data, color) ->
        # Set current params
        Tree.currentNode = currentNode
        Tree.currentNodeLevel = Tree.depth
        Tree.depth++
        Tree.stage.setHeight(Tree.stage.getHeight() + 164) if Tree.depth > 1
        interval = 0
        node_space = if data.length < 10 then SPACE else SPACE / 2

        # Define X coordinate to start drawing
        x_start = currentNode.getChildren()[0].getX() - (node_space + (RADIUS + 3)) * (data.length - 1)

        # Create a group for children
        children_group = new Kinetic.Group()

        # Iterate through data and create node + label + branch
        for i in [0..data.length - 1]
            # Create group
            branch_group = new Kinetic.Group()

            # Create node
            node_group = Tree.createNode(x_start + interval, (currentNode.getChildren()[0].getY() + 164), (data[i].course_id).toString())

            # Bind events on node
            node_group.on 'mouseover', () ->
                document.body.style.cursor = "pointer"
                this.getChildren()[0].setStroke(color)
                this.getChildren()[0].setStrokeWidth(5)
                this.getParent().getChildren()[0].setStrokeWidth(5)
                this.getChildren()[1].setFill(color)
                Tree.layer.draw()

            node_group.on 'mouseout', () ->
                document.body.style.cursor = "default"
                this.getChildren()[0].setStroke("#999")
                this.getChildren()[0].setStrokeWidth(3)
                this.getParent().getChildren()[0].setStrokeWidth(3)
                this.getChildren()[1].setFill("#222")
                Tree.layer.draw()

            # Draw branch
            branch = new Kinetic.Line(
                points: [node_group.getChildren()[0].getX(), node_group.getChildren()[0].getY(), currentNode.getChildren()[0].getX(), currentNode.getChildren()[0].getY()]
                stroke: color
                strokeWidth: 3
                lineCap: 'round'
                lineJoin: 'round'
            )

            branch_group.add(branch)
            branch_group.add(node_group)
            children_group.add(branch_group)

            # Update interval between node
            interval += (node_space*2) + (RADIUS + 3)*2

        # Layer redraw
        currentNode.add(children_group)
        children_group.moveToBottom()
        Tree.layer.draw()

    @createNode: (pos_x, pos_y, key) ->
        #Create a Kinetic Group instance which contains a Circle and Label instances.
        node_group = new Kinetic.Group(id: Tree.depth + "_" + key)

        leaf = new Kinetic.Circle(
            x: pos_x
            y: pos_y
            radius: RADIUS
            fill: '#ddd'
            stroke: '#999'
            strokeWidth: 3
        )

        # Add label into node (id)
        label = new Kinetic.Text(
            x: if key.length > 1 then leaf.getX() - 16 else leaf.getX() - 8
            y: leaf.getY() - 16
            text: key
            fontSize: 32
            fontFamily: 'Calibri'
            fill: '#222'
        )

        node_group.add(leaf)
        node_group.add(label)

        # Return new group instance
        node_group

    @addLegend: (target, data) ->
        # Create legend item
        item = "<li class=\"legend-item\" data-node=\"" + target + "\"></li>"

        # Add target title
        item = $(item).append("<h4 class=\"" + target+ "\">" + target.toUpperCase() + "</h4>")[0]

        # Add legend item container
        titles = "<ul class=\"unstyled\" id=\"legend-item-container\"></ul>"

        for key, value of data
            titles = $(titles).append("<li class=\"legend-item-title\"><span class=\"legend-title\"><strong><span class=\"legend-number " + target + "\">#{key}</span> : #{value}</strong></span></li>")[0]
        item = $(item).append(titles)[0]
        Tree.legend = $(Tree.legend).append(item)[0]

    @removeLegend: (target) ->
        $(Tree.legend).children('li').each (i, item) ->
            $(item).remove() if $(item).data('node') is target

    @displayCourse: (target, data) ->
        # Get the tbody of course table
        tbody = $(Tree.course).find('tbody')

        # Fill in it
        for i in [0..data.length - 1]
            tr = "<tr></tr>"
            for key, value of data[i]
                switch key
                    when 'course_id' then tr = $(tr).append("<td class=\"course\">#{value}</td>")[0]
                    when 'cycle' then tr = $(tr).append("<td class=\"cycle\">#{value}</td>")[0]
                    when 'domain' then tr = $(tr).append("<td class=\"domain\">#{value.domain_id}</td>")[0]
                    when 'professors'
                        td = "<td class=\"professor\"></span>"
                        for j in [0..value.length - 1]
                            td = $(td).append("<span data-professor-id=#{value[j].professor_id}>#{value[j].fname} #{value[j].name}</span>")[0]
                        tr = $(tr).append(td)[0]
                    else tr = $(tr).append("<td>#{value}</td>")[0]
            tbody = $(tbody).append(tr)[0]

        # Display course table
        $(Tree.course).show()

    @removeCourse: () ->
        # Get the tbody of course table
        $(Tree.course).find('tbody').children('tr').each (i, item) ->
            $(item).remove()

        # Hide course table
        $(Tree.course).hide()

    @displayError: (msg) ->
        $(Tree.error).html("No data found").fadeIn(200)
        $(Tree.error).fadeOut 4000, () ->
            $(Tree.error).html("")