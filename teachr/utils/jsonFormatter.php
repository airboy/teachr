<?php

/* Format PHP Arrays to JSON */

function getJSONFaculty($data) {
    $res = "{";
    for ($i = 0; $i < count($data); $i++) {
        $res .= "\"" . $data[$i]['faculty_id'] . "\": \"" . $data[$i]['title'] . "\"";
        $res .= ($i < count($data) - 1 ? ", " : "");
    }
    $res .= "}";
    return $res;
}

function getJSONDomain($data) {
    $res = "{";
    for ($i = 0; $i < count($data); $i++) {
        $res .= "\"" . $data[$i]['domain_id'] . "\": \"" . $data[$i]['title'] . "\"";
        $res .= ($i < count($data) - 1 ? ", " : "");
    }
    $res .= "}";
    return $res;
}

function getJSONProfessor($data) {
    $res = "{";
    for ($i = 0; $i < count($data); $i++) {
        $res .= "\"" . $data[$i]['professor_id'] . "\": ";
        $res .= "{";
        $res .= "\"name\": \"" . $data[$i]['name'] . "\", ";
        $res .= "\"fname\": \"" . $data[$i]['fname'] . "\", ";
        $res .= "\"email\": \"" . $data[$i]['email'] . "\"";
        $res .= "}";
        $res .= ($i < count($data) - 1 ? ", " : "");
    }
    $res .= "}";
    return $res;
}

function getJSONCourse($data) {
    $res = "[";
    for ($i = 0; $i < count($data); $i++) {
        $res .= "{";
        $res .= "\"course_id\": " . $data[$i]['course_id'] . ", ";
        $res .= "\"title\": \"" . $data[$i]['title'] . "\", ";
        $res .= "\"langue\": \"" . $data[$i]['langue'] . "\", ";
        $res .= "\"cycle\": \"" . $data[$i]['cycle'] . "\", ";
        $res .= "\"level\": " . $data[$i]['level'] . ", ";

        // Get domain
        $domain = teachrDao::getDomain($data[$i]['target_domain']);
        $res .= "\"domain\": {\"domain_id\" : \"" . $domain['title'] . "\"}, ";

        // Get professors
        $res .= "\"professors\": ";
        $res .= "[";
        $professors = teachrDao::getProfessorsByCourse($data[$i]['course_id']);
        for ($j = 0; $j < count($professors); $j++) {
            $res .= "{\"professor_id\": " . $professors[$j]['professor_id'] . ", ";
            $res .= "\"name\": \"" . $professors[$j]['name'] . "\", ";
            $res .= "\"fname\": \"" . $professors[$j]['fname'] . "\"";
            $res .= ($j < count($professors) - 1 ? "}, " : "}");
        }
        $res .= "]";
        $res .= ($i < count($data) - 1 ? "}, " : "}");
    }
    $res .= "]";
    return $res;
}

?>