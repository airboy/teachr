### PHP Project : Teach'r ###

PHP Object - MVC

### Teach'r data structure ###

The goal fo this project is to use the power of Javascript 2D Framework "[KineticJS](http://kineticjs.com/)" to draw a tree representation of data (Canvas).

In this case, professors may be linked to different domains such as Science, Medecine, Physic in which we can find a variety of courses. We can navigate within the arborescence or find links between structures. For example, it will be possible to determine all courses for a specific teacher which belongs to a specific faculty.

### Tree visualization ###
![Screenshot](https://s3-eu-west-1.amazonaws.com/teachr/tree.png)